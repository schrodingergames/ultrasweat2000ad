﻿using UnityEngine;
using System.Collections;

public class CenterOfMassAdjuster : MonoBehaviour
{

    [SerializeField]
    Vector2 centerOfMass;
    Rigidbody2D rigid;
    // Use this for initialization
    void Start()
    {

        rigid = GetComponent<Rigidbody2D>();
        rigid.centerOfMass = centerOfMass;

    }

    void Update()
    {
        rigid.centerOfMass = centerOfMass;
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawSphere(transform.TransformPoint(centerOfMass), 0.3f);
    }
}
