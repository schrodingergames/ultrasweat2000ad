﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraFollow : MonoBehaviour 
{
	[SerializeField] List<Dragger> draggers;

    [SerializeField] float dumping;
    [SerializeField] Vector3 offsetsFollowPoint;
	[SerializeField] Vector3 finalOffset;


	[SerializeField] Transform lerpSand;

    Vector3 velocity = new Vector3();
	// Update is called once per frame

    
	Transform follow;

	void Update () 
	{
		if(follow != null)
		{
			Vector3 pos = Vector3.SmoothDamp(transform.position, follow.position + offsetsFollowPoint, ref velocity, dumping) + finalOffset;
			pos.z = -10f;
			transform.position = pos;
		}
		else
		{
			if(draggers.Count > 0)
			{
				GetDraggerToFollow();	
			}
		}

		Vector3 p = lerpSand.position;
		p.y = transform.position.y;
		lerpSand.position = p;
	}

	void GetDraggerToFollow()
	{
		for(int i = draggers.Count - 1; i >= 0; i--)
		{
			if(draggers[i] == null)
			{
				draggers.RemoveAt(i);
			}
		}

		if(draggers.Count > 0)
		{
			follow = draggers[UnityEngine.Random.Range(0, draggers.Count)].transform;
		}
	}
}
