﻿using UnityEngine;
using System.Collections;
using System;

public class ChainLink : MonoBehaviour {

    [SerializeField]
    public HingeJoint2D joint;
    [SerializeField]
    public Rigidbody2D rigid;
    [SerializeField]
    public float length;

    internal void AttachtheAnchor(Rigidbody2D previousRB, bool first)
    {
        joint.connectedBody = previousRB;
        joint.connectedAnchor = new Vector2(0.0f, first? 0:  length);
    }

    void OnDrawGizmos()
    {
        if (joint.connectedBody != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(joint.connectedBody.transform.position, transform.position);
        }
    }
}
