﻿using UnityEngine;
using System.Collections;

public class MoverScript : MonoBehaviour {

    [SerializeField]
    GameObject chainLinkPrefab;

    [SerializeField]
    int numLinks;

    [SerializeField]
    GameObject chainStart;

    [SerializeField]
    ChainLink chainEnd;

	// Use this for initialization
	void Start () {

        GameObject go = new GameObject();
        go.name = "GeneratedRope";
        Rigidbody2D previousRB = chainStart.GetComponent<Rigidbody2D>();
        for (int i=0; i < numLinks;i++)
        {
            GameObject chLink = Instantiate(chainLinkPrefab.gameObject);
            ChainLink link = chLink.GetComponent<ChainLink>();
            link.AttachtheAnchor(previousRB, i==0);
            previousRB = chLink.GetComponent<Rigidbody2D>();
            chLink.transform.SetParent(go.transform);
        }

        chainEnd.AttachtheAnchor(previousRB, true);
	}
}
