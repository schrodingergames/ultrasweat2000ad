﻿using UnityEngine;
using System.Collections;

public class Dragger : MonoBehaviour {

    [SerializeField]
    float force;
    [SerializeField]
    float maxVelocity;
    [SerializeField]
    float minForce;

    [SerializeField]
    float minF;

    [SerializeField]
    float forceGrowSpeed = 10.0f;
    Rigidbody2D rigid;

    [SerializeField]
    Vector2 cast0;
    [SerializeField]
    Vector2 cast1;
    int layeMask;
    int layerGroundName;
    int rockLayerMask;
    [SerializeField]
    float deadlyVelocity;

	void Start()
    {
        minF = minForce;
        cast0 = cast0.normalized;
        cast1 = cast1.normalized;
        layerGroundName = LayerMask.NameToLayer("ground");
        rockLayerMask = LayerMask.NameToLayer("rock");

        layeMask = LayerMask.GetMask(new string[] { "ground"});
        rigid = GetComponent<Rigidbody2D>();
    }
    [SerializeField]
    Vector2 right;
	// Update is called once per frame
	void FixedUpdate () {

       
        RaycastHit2D hit0 = Physics2D.Raycast(transform.position, cast0, 1000.0f, layeMask);
        RaycastHit2D hit1 = Physics2D.Raycast(transform.position, cast1, 1000.0f, layeMask);

        right = hit0.point - hit1.point;
        right.Normalize();
        right += Vector2.down * 0.2f;
        right.Normalize();
        if (Input.GetKey(KeyCode.RightArrow) )
        {
            float f = minF;
            if (rigid.velocity.x > 0.0f)
            {
                f = Mathf.Lerp(minF, force, Mathf.Clamp01(rigid.velocity.magnitude / maxVelocity));
            }           

            rigid.AddForce(f * right * Time.fixedDeltaTime, ForceMode2D.Impulse);
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            float f = minF;
            if (rigid.velocity.x < 0.0f)
            {
                f = Mathf.Lerp(minF, force, Mathf.Clamp01(rigid.velocity.magnitude / maxVelocity));
            }
            rigid.AddForce(-f * right * Time.fixedDeltaTime, ForceMode2D.Impulse);
        }
       
        if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow))
        {
            if(rigid.velocity.magnitude < maxVelocity)
            {
                minF += forceGrowSpeed * Time.fixedDeltaTime;
            }
            else
            {
                minF = minForce;
            }
        }
        else
        {
            minF = minForce;
        }

	}
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + new Vector3( right.x, right.y, 0.0f) * 10.0f);   
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.attachedRigidbody == null) 
			return;
		
        Vector2 dv = other.attachedRigidbody.velocity;

        if(other.gameObject.layer == rockLayerMask && dv.magnitude > deadlyVelocity)
        {
			Debug.Log("Collision Velocity:"+dv.magnitude);
//			Debug.Log("vel = " + dv + " | deadlyVel = " + deadlyVelocity);
            Destroy(gameObject);
        }
    }


}
