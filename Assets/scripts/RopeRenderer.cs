﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RopeRenderer : MonoBehaviour {

    [SerializeField]
    float maxSlackDistance = 10;
    [SerializeField]
    float pointsOffset = 3.0f;
    [SerializeField]
    string layerName;
    DistanceJoint2D distanceJoint;
    int layeMask;
    MeshRenderer render;
    MaterialPropertyBlock propertyBlock;
    int p0p1, p2p3;
    // Use this for initialization
    void Start () {
        layeMask = LayerMask.GetMask(new string[] { "ground" });
        distanceJoint = GetComponent<DistanceJoint2D>();

        render = GetComponent<MeshRenderer>();
        
        propertyBlock = new MaterialPropertyBlock();
        render.GetPropertyBlock(propertyBlock);
        p0p1 = Shader.PropertyToID("p0p1");
        p2p3 = Shader.PropertyToID("p2p3");
        List<Vector3> positions = new List<Vector3>();
        List<int> triangels = new List<int>();
        for (int i=0; i < 10; i++)
        {
            float t = i / ((float)9);
            positions.Add(new Vector3(t, 0.0f, 0.0f));            
            positions.Add(new Vector3(t, 1.0f, 0.0f));
            
        }

        for(int i=1; i < 10; i++)
        {
            triangels.Add((i-1)*2);
            triangels.Add((i-1)*2 + 1);
            triangels.Add((i) * 2 + 1);

            triangels.Add((i - 1) * 2 );
            triangels.Add((i) * 2 + 1);
            triangels.Add((i) * 2);
        }

        Mesh m = new Mesh();
        m.name = "generatedDragger";
        m.SetVertices(positions);
        m.SetIndices(triangels.ToArray(), MeshTopology.Triangles, 0);
        GetComponent<MeshFilter>().mesh = m;
        render.sortingLayerName = layerName;
    }

    [SerializeField]
    float ratio;

    Vector2 a0, a1, a2, a3;
    Vector2 dir;
    [SerializeField]
    Vector2 N;

    [SerializeField]
    Material ropeMaterial;

    // Update is called once per frame
    void Update () {
        Vector2 otherBodyPos = distanceJoint.connectedBody.transform.position;
        otherBodyPos += distanceJoint.connectedAnchor;

        Vector2 thisBodyPos = transform.position;
        thisBodyPos += distanceJoint.anchor;
        a0 = otherBodyPos;
        a3 = thisBodyPos;

        float realDistance = Vector2.Distance(thisBodyPos, otherBodyPos);
         
        ratio =  realDistance/ distanceJoint.distance;

        dir = (thisBodyPos - otherBodyPos).normalized;
        N = new Vector2(dir.y, -dir.x);
        if (N.y > 0.0f) N = -N;
        if (Mathf.Abs( N.x )> Mathf.Abs( N.y) * 0.6f) N = Vector2.down;

        if(pointsOffset*2 > realDistance)
        {
            a1 = a0 + dir * (realDistance / 2.0f);
            a2 = a1;
        }
        else
        {
            a1 = a0 + dir * (pointsOffset);
            a2 = a3 - dir * (pointsOffset);
        }

        RaycastHit2D hitA = Physics2D.Raycast(a1, N, 1000.0f, layeMask);
        RaycastHit2D hitB = Physics2D.Raycast(a2, N, 1000.0f, layeMask);

        if(hitA.collider != null)
        {
            a1 = Vector2.Lerp( hitA.point, a1, ratio);
        }
        if(hitB.collider != null)
        {
            a2 = Vector2.Lerp(hitB.point, a2, ratio);
        }

        propertyBlock.SetVector(p0p1, new Vector4(a0.x,a0.y, a1.x,a1.y));
        propertyBlock.SetVector(p2p3, new Vector4(a2.x, a2.y, a3.x, a3.y));
        render.SetPropertyBlock(propertyBlock);

    }

    Vector2 CatmullRoma(Vector2 a0,Vector2 a1, Vector2 a2, Vector2 a3, float t)
    {        

        Vector3 a = 0.5f * (2f * a1);
        Vector3 b = 0.5f * (a2 - a0);
        Vector3 c = 0.5f * (2f * a0 - 5f * a1 + 4f * a2 - a3);
        Vector3 d = 0.5f * (-a0 + 3f * a1 - 3f * a2 + a3);

        Vector3 pos = a + (b * t) + (c * t * t) + (d * t * t * t);

        return pos;
    }


    Vector3 BezierLine(Vector2 a0, Vector2 a1, Vector2 a2, Vector2 a3, float t)
    {
        float tminOne = (1 - t);
        float tminOne2 = tminOne* tminOne;
        float tminOne3 = tminOne * tminOne * tminOne;

        Vector3 ret = a0 * (tminOne3) + 3 * a1 * tminOne2 * t + 3 * t * t * tminOne * a2 + t * t * t*a3;
        return ret;
    }
    void DrawGizmoSpline(Vector2 a0, Vector2 a1, Vector2 a2, Vector2 a3)
    {
        Gizmos.color = Color.white;
        Vector2 previous = new Vector2();
        for (int i = 0; i < 10; i++)
        {
            float t = i / ((float)9);
            Vector2 p = BezierLine(a0, a1, a2, a3, t);

            if (i > 0)
            {
                Gizmos.DrawLine(previous, p);
            }
            previous = p;
        }
    }
/*
    void OnDrawGizmos()
    {
        //DrawGizmoSpline(a0 + (a0-a1), a0, a1, a2);
        DrawGizmoSpline(a0, a1, a2, a3);
        //DrawGizmoSpline(a1, a2, a3, a3+ (a3-a2));

        Gizmos.DrawSphere(a0, 0.2f);
        Gizmos.DrawSphere(a3, 0.2f);

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(a1, 0.2f);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(a2, 0.2f);
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(a0 + (a0 - a1), 0.2f);
        Gizmos.color = Color.magenta;
        Gizmos.DrawSphere(a3 + (a3 - a2), 0.2f);

        /*Gizmos.color = Color.yellow;
        Gizmos.DrawLine(a0, a1);
        Gizmos.DrawLine(a1, a2);
        Gizmos.DrawLine(a2, a3);
        
    }
*/
}
