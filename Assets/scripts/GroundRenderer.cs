﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class GroundRenderer : MonoBehaviour {

    [SerializeField]
    EdgeCollider2D dgecollider;
    [SerializeField]
    Vector3 oneOffset;
    [SerializeField]
    Vector3 twooffset;

    [SerializeField]
    string layerName;
	// Use this for initialization
	void Start ()
    {
        List<Vector3> positions = new List<Vector3>();
        var cp = dgecollider.points;

        float length = 0.0f;
        for(int i=0; i < cp.Length; i++)
        {
            positions.Add(cp[i]);
            if( i > 0)
            {
                length += Vector2.Distance(cp[i - 1], cp[i]);
            }
        }

        positions.Sort((x, y) => { return x.x.CompareTo(y.x); });

        List<Vector3> final = new List<Vector3>();
        List<Vector2> uv = new List<Vector2>();
        float lengthAcc = 0.0f;
        for(int i=0; i < positions.Count; i++)
        {
            final.Add(positions[i]);
            final.Add(positions[i] + oneOffset);
            final.Add(positions[i] + twooffset);

            float t = 0.0f;
            if(i > 0)
            {
                lengthAcc += Vector2.Distance(positions[i-1], positions[i]);
                t = lengthAcc / length;
            }
            

            uv.Add(new Vector2(t, 0));
            uv.Add(new Vector2(t, 0.15f));
            uv.Add(new Vector2(t, 1f));
        }

        List<int> inxs = new List<int>();

        for (int i = 1; i < positions.Count; i++)
        {
            inxs.Add((i - 1) * 3);
            inxs.Add((i - 1) * 3 + 1);
            inxs.Add((i) * 3 + 1);

            inxs.Add((i - 1) * 3);
            inxs.Add((i) * 3 + 1);
            inxs.Add((i) * 3);



            inxs.Add((i - 1) * 3 +1);
            inxs.Add((i - 1) * 3 + 2);
            inxs.Add((i) * 3 + 2);

            inxs.Add((i - 1) * 3 +1);
            inxs.Add((i) * 3 + 2);
            inxs.Add((i) * 3 +1);
        }
        Mesh m = new Mesh();
        m.name = "Reconstructed Ground";
        m.SetVertices(final);
        m.SetUVs(0, uv);
        m.SetIndices(inxs.ToArray(), MeshTopology.Triangles, 0);
        GetComponent<MeshFilter>().sharedMesh = m;

        GetComponent<MeshRenderer>().sortingLayerName = "ground"; 

    }

    
}
