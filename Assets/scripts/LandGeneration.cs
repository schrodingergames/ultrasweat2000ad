﻿using UnityEngine;
using System.Collections;

public class LandGeneration : MonoBehaviour {

    [SerializeField]
    int numLinks;
    [SerializeField]
    int subLinks;
    [SerializeField]
    float xStep;

    [SerializeField]
    float amp;
    [SerializeField]
    float freq;

	// Use this for initialization
	void Start () {
        float xPos = 0;
        Vector2 prevPos = new Vector2();
	    for(int i=0; i < numLinks; i++)
        {
            GameObject go = new GameObject();
            go.transform.SetParent(transform);
            go.layer = LayerMask.NameToLayer("ground");
            EdgeCollider2D collider = go.AddComponent<EdgeCollider2D>();
            Vector2 [] points =  new Vector2[subLinks];
            points[0] = prevPos;
            for (int si=1; si< subLinks; si++)
            {
                xPos += xStep;
                Vector2 pos = new Vector2(xPos, amp * Mathf.Sin(freq * xPos));
                points[si] = pos;
                Debug.Log(pos);
                prevPos = pos;
            }
          

            collider.points = points;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
