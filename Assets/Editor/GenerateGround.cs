﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class GenerateGround : EditorWindow
{

    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/GroundMaker")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        GenerateGround window = (GenerateGround)EditorWindow.GetWindow(typeof(GenerateGround));
        window.Show();
    }
    Mesh mesh;

    int numSublinks;
    void OnGUI()
    {

        mesh = (Mesh)EditorGUILayout.ObjectField("Mesh", mesh, typeof(Mesh), true);
        numSublinks = EditorGUILayout.IntField("Num Sub Links", numSublinks);
        if (GUILayout.Button("Apply Scale"))
        {
            GameObject go = (GameObject)Selection.activeObject;
            var oldPoints = go.GetComponent<EdgeCollider2D>().points;

            GameObject ngo = new GameObject();

            var ecol = ngo.AddComponent<EdgeCollider2D>();

            List<Vector2> points = new List<Vector2>();

            for(int i=0; i < oldPoints.Length; i++ )
            {
                points.Add(go.transform.TransformPoint(oldPoints[i]));
            }
            ecol.points = points.ToArray();
        }
            if (GUILayout.Button("Generate Ground"))
        {
            GameObject go = new GameObject();
            go.name = "Generated ground";

            Vector3[] vert = mesh.vertices;
            List<Vector2> colliders = new List<Vector2>();

            for (int i = 0; i < vert.Length; i++)
            {
                colliders.Add(new Vector2(vert[i].x, vert[i].y));
            }

            colliders.Sort((Vector2 pos0, Vector2 pos1) => { return pos0.x.CompareTo(pos1.x); });

            int inOneObj = numSublinks;
            Vector2[] points = null;
            EdgeCollider2D collider = null;
            for (int c = 0; c < colliders.Count; c++)
            {
                inOneObj++;
                if (inOneObj >= numSublinks)
                {
                    inOneObj = 0;
                }

                if (inOneObj == 0)
                {
                    GameObject subObj = new GameObject();
                    subObj.transform.SetParent(go.transform);
                    subObj.layer = LayerMask.NameToLayer("ground");
                    if (collider != null)
                    {
                        collider.points = points;
                    }
                    collider = subObj.AddComponent<EdgeCollider2D>();
                    subObj.name = "GeneratedSubGround";
                    points = new Vector2[numSublinks];
                    if(c > 0)
                    {
                        c--;
                    }

                }

                points[inOneObj] = colliders[c];
            }
            if (collider != null)
            {
                collider.points = points;
            }
        }
    }



}
