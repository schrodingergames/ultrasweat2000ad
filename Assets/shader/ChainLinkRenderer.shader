﻿Shader "Unlit/ChainLinkRenderer"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		p0p1("p0p1", Vector) = (0,0,1,0)
		p2p3("p2p3", Vector) = (2,0,3,0)
	    width("width", Float) = 0.5
		scaleX("scaleX", Float) = 0.5
	}
	SubShader
	{
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "DisableBatching" = "True" "IgnoreProjector" = "True" }
		LOD 100
			
		Pass
		{
			Cull Off
			ZWrite Off
			ZTest Always
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"
			 
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 p0p1;
			float4 p2p3;
			float width;
			float scaleX;
			v2f vert (appdata v)
			{
				v2f o;
				float4 zeropos  = mul(_World2Object, v.vertex);
				
				UNITY_TRANSFER_FOG(o,o.vertex);
				float2 p0 = p0p1.xy;
				float2 p1 = p0p1.zw;
				float2 p2 = p2p3.xy;
				float2 p3 = p2p3.zw;

				float t = v.vertex.x;
				float oneMinT = 1 - t;

				float2 p = p0*oneMinT*oneMinT*oneMinT + 3 * p1*oneMinT*oneMinT*t + 3 * t*t*oneMinT*p2 + t*t*t*p3;

				p.y += v.vertex.y*width;
				o.uv = float2((p.x + zeropos.x)*scaleX ,v.vertex.y);//TRANSFORM_TEX(v.vertex.xy, _MainTex);
				float4 vert = float4(p,0.0,1.0);
				o.vertex = mul(UNITY_MATRIX_VP, vert);
				
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
